<?php
/**
 * 进行必要的配置，包含需要的文件
 *
 * @author langcai1943
 */
define('GLOBAL_DEBUG', 0); /* 0 or 1, 定义调试宏变量 */
if(GLOBAL_DEBUG){
    @ini_set("display_errors", "on"); /* @表示出错信息不回显；对PHP进行配置。
                                         这个选项会在脚本运行时保持新的值，并在脚本
                                         结束时恢复。 */
    @error_reporting(E_ALL^E_NOTICE^E_WARNING); /* PHP规定不同的错误级别报告 */
}else{
    @ini_set("display_errors", "on");
    @error_reporting(E_ALL^E_NOTICE^E_WARNING);
}

header("Content-type: text/html; charset=utf-8"); /* PHP向客户端发送原始的HTTP报头 */
    /* 1. 从该config.php文件回溯到服务器根目录；
       2. str_replace是PHP替换字符串的函数；
       3. dirname是PHP返回路径中的目录部分；
       4. __FILE__是PHP魔术常量，是文件的完整路径和文件名；
       5. .句点是PHP字符串拼接操作符。 */
define('BASIC_PATH', str_replace('\\', '/', dirname(dirname(__FILE__))).'/');
define('LIB_DIR',       BASIC_PATH .'app/');        //系统库目录
define('PLUGIN_DIR',    BASIC_PATH .'plugins/');    //插件目录
define('CONTROLLER_DIR',LIB_DIR .'controller/');    //控制器目录
define('FUNCTION_DIR',  LIB_DIR .'function/');      //函数库目录
define('CLASS_DIR',     LIB_DIR .'kod/');           //工具类目录
define('CORER_DIR',     LIB_DIR .'core/');          //核心目录

//include(FUNCTION_DIR.'common.function.php');
//include(FUNCTION_DIR.'web.function.php');
//include(FUNCTION_DIR.'file.function.php');
include(FUNCTION_DIR.'helper.function.php');
//include(CLASS_DIR.'I18n.class.php');
//include(BASIC_PATH.'config/version.php');

include(CONTROLLER_DIR.'utils.php');
//include(BASIC_PATH.'config/setting.php');

//init_common(); /* 位于helper.function.php，检查要使用的文件夹是否存在 */
$config['autorun'] = array(
        array('controller'=>'user','function'=>'loginCheck'),
        array('controller'=>'user','function'=>'authCheck'),
        array('controller'=>'user','function'=>'bindHook'),
);

