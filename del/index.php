<?php
/**
 * php入口文件
 *
 * 仅仅是个人学习php时，按照从简单到复杂的原则慢慢上传的代码，没有什么规划，
 * 也没有什么目的，你也可以随便上传，只要保证不删除我的master分支，程序能正常运行就可以了。
 * 使用phpDocumentor注释风格。
 * @author  langcai1943
 * @version 0.1
 */

    ob_start(); /* ob_start(callback)，与ob_end_flush()配合使用，使php将内容先存储到
                   output buffer，最后调用callback（可用于存文件或者打包）后再一次性
                   输出到前端 */
    include ('config/config.php'); /* 包含文件 */

    include(CORER_DIR.'Application.class.php'); /* 这应该在哪个加密文件中加载的，因为全局搜索没搜到 */
    $app = new Application(); /* new一个新class */
//    init_config();
    $app->run(); /* 运行类中一个方法 */
    echo("<br>Filish the test.<br>");
?>
