<?php
$GLOBALS["md5"] = "md5"; 
$GLOBALS["json_encode"] = "json_encode"; 
$GLOBALS["json_decode"] = "json_decode"; 
$GLOBALS["base64_encode"] = "base64_encode"; 
$GLOBALS["file_get_contents"] = "file_get_contents"; 
$GLOBALS["in_array"] = "in_array"; 
$GLOBALS["implode"] = "implode"; 
$GLOBALS["explode"] = "explode";
$GLOBALS["explode"] = "explode"; 
$GLOBALS["header"] = "header"; 
$GLOBALS["strtotime"] = "strtotime"; 
$GLOBALS["strlen"] = "strlen"; 
$GLOBALS["trim"] = "trim"; 
$GLOBALS["str_replace"] = "str_replace"; 
$GLOBALS["rawurlencode"] = "rawurlencode"; 
$GLOBALS["substr"] = "substr"; 
$GLOBALS["time"] = "time"; 
$GLOBALS["file_put_contents"] = "file_put_contents"; 
$GLOBALS["file_exists"] = "file_exists"; 
$GLOBALS["preg_replace"] = "preg_replace"; 
$GLOBALS["session_start"] = "session_start";
$GLOBALS["session_name"] = "session_name"; 
define("KOD_GROUP_PATH", "{groupPath}"); 
define("KOD_GROUP_SHARE", "{groupShare}"); 
define("KOD_USER_SELF", "{userSelf}"); 
define("KOD_USER_SHARE", "{userShare}"); 
define("KOD_USER_RECYCLE", "{userRecycle}"); 
define("KOD_USER_FAV", "{userFav}"); 
define("KOD_GROUP_ROOT_SELF", "{treeGroupSelf}"); 
define("KOD_GROUP_ROOT_ALL", "{treeGroupAll}"); 

function _DIR_CLEAR($path) 
{
	$path = $GLOBALS["str_replace"]('\\', '/', $path);
	$path = $GLOBALS["preg_replace"]("/\/+/", "/", $path);
	$zym_var_26 = $path;
	if (isset($GLOBALS["isRoot"]) && $GLOBALS["isRoot"]) 
	{
		return $path;
	}
	$zym_var_25 = "/../";
	if ($GLOBALS["substr"]($path, 0, 3) == "../") 
	{
		$path = $GLOBALS["substr"]($path, 3);
	}
	while (strstr($path, $zym_var_25)) 
	{
		$path = $GLOBALS["str_replace"]($zym_var_25, "/", $path);
	}
	$path = $GLOBALS["preg_replace"]("/\/+/", "/", $path);
	return $path;
}

function _DIR($zym_var_27) 
{
	$path = _DIR_CLEAR($zym_var_27);
	$path = iconv_system($path);
	$zym_var_31 = array(KOD_GROUP_PATH, KOD_GROUP_SHARE, KOD_USER_SELF, KOD_GROUP_ROOT_SELF, KOD_GROUP_ROOT_ALL, KOD_USER_SHARE, KOD_USER_RECYCLE, KOD_USER_FAV);
	$GLOBALS["kodPathType"] = '';
	$GLOBALS["kodPathPre"] = HOME;
	$GLOBALS["kodPathI"."d"] = '';
	unset($GLOBALS["kodPathIdShare"]);
	foreach ($zym_var_31 as $zym_var_29) 
	{
		if ($GLOBALS["substr"]($path, 0, $GLOBALS["strlen"]($zym_var_29)) == $zym_var_29) 
		{
			$GLOBALS["kodPathType"] = $zym_var_29;
			$zym_var_30 = $GLOBALS["explode"]("/", $path);
			$zym_var_33 = $zym_var_30[0];
			unset($zym_var_30[0]);
			$zym_var_38 = $GLOBALS["implode"]("/", $zym_var_30);
			$zym_var_39 = $GLOBALS["explode"](":", $zym_var_33);
			if ($GLOBALS["explode"]($zym_var_39) > 1) 
			{
				$GLOBALS["kodPathId"] = $GLOBALS["trim"]($zym_var_39[1]);
			}
			else 
			{
				$GLOBALS["kodPathId"] = '';
			}
			break;
		}
	}
	switch ($GLOBALS["kodPathType"]) 
	{
		case '': $path = iconv_system(HOME) . $path;
		break;
		case KOD_USER_RECYCLE: $GLOBALS["kodPathPre"] = $GLOBALS["trim"](USER_RECYCLE, "/");
		$GLOBALS["kodPathId"] = '';
		return iconv_system(USER_RECYCLE) . "/" . str_replace(KOD_USER_RECYCLE, '', $path);
		case KOD_USER_SELF: $GLOBALS["kodPathPre"] = $GLOBALS["trim"](HOME_PATH, "/");
		$GLOBALS["kodPathId"] = '';
		return iconv_system(HOME_PATH) . "/" . str_replace(KOD_USER_SELF, '', $path);
		case KOD_USER_FAV: $GLOBALS["kodPathPre"] = $GLOBALS["trim"](KOD_USER_FAV, "/");
		$GLOBALS["kodPathId"] = '';
		return KOD_USER_FAV;
		case KOD_GROUP_ROOT_SELF: $GLOBALS["kodPathPre"] = $GLOBALS["trim"](KOD_GROUP_ROOT_SELF, "/");
		$GLOBALS["kodPathId"] = '';
		return KOD_GROUP_ROOT_SELF;
		case KOD_GROUP_ROOT_ALL: $GLOBALS["kodPathPr"."e"] = $GLOBALS["trim"](KOD_GROUP_ROOT_ALL, "/");
		$GLOBALS["kodPathI"."d"] = '';
		return KOD_GROUP_ROOT_ALL;
		case KOD_GROUP_PATH: $zym_var_36 = systemGroup::getInfo($GLOBALS["kodPathId"]);
		if (!$GLOBALS["kodPathId"] || !$zym_var_36) 
		{
			return false;
		}
		owner_group_check($GLOBALS["kodPathI"."d"]);
		$GLOBALS["kodPathPre"] = group_home_path($zym_var_36);
		$path = iconv_system($GLOBALS["kodPathPre"]) . $zym_var_38;
		break;
		case KOD_GROUP_SHARE: $zym_var_36 = systemGroup::getInfo($GLOBALS["kodPathId"]);
		if (!$GLOBALS["kodPathI"."d"] || !$zym_var_36) 
		{
			return false;
		}
		owner_group_check($GLOBALS["kodPathId"]);
		$GLOBALS["kodPathPre"] = group_home_path($zym_var_36) . $GLOBALS["config"]["settingSystem"]["groupShare"."Folder"] . "/";
		$path = iconv_system($GLOBALS["kodPathPre"]) . $zym_var_38;
		break;
		case KOD_USER_SHARE: $zym_var_36 = systemMember::getInfo($GLOBALS["kodPathId"]);
		if (!$GLOBALS["kodPathId"] || !$zym_var_36) 
		{
			return false;
		}
		if ($GLOBALS["kodPathId"] != $_SESSION["kodUser"]["userID"]) 
		{
			$zym_var_37 = $GLOBALS["config"]["pathRoleG"."roupDefault"]["1"]["actions"];
			path_role_check($zym_var_37);
		}
		$GLOBALS["kodPathPre"] = '';
		$GLOBALS["kodPathIdS"."hare"] = $zym_var_27;
		if ($zym_var_38 == '') 
		{
			return $path;
		}
		else 
		{
			$zym_var_32 = $GLOBALS["explode"]("/", $zym_var_38);
			$zym_var_32[0] = iconv_app($zym_var_32[0]);
			$zym_var_34 = systemMember::userShareGet($GLOBALS["kodPathId"], $zym_var_32[0]);
			$GLOBALS["kodShareIn"."fo"] = $zym_var_34;
			$GLOBALS["kodPathI"."dShare"] = KOD_USER_SHARE . ":" . $GLOBALS["kodPathId"] . "/" . $zym_var_32[0] . "/";
			unset($zym_var_32[0]);
			if (!$zym_var_34) 
			{
				return false;
			}
			$zym_var_35 = rtrim($zym_var_34["path"], "/") . "/" . iconv_app($GLOBALS["implode"]("/", $zym_var_32));
			if ($zym_var_36["role"] != "1") 
			{
				$zym_var_36 = user_home_path($zym_var_36);
				$GLOBALS["kodPathPre"] = $zym_var_36 . rtrim($zym_var_34["path"], "/") ."/";
				$path = $zym_var_36 . $zym_var_35;
			}
			else 
			{
				$GLOBALS["kodPathPre"] = $zym_var_34["path"];
				$path = $zym_var_35;
			}
			if ($zym_var_34["type"] == "file") 
			{
				$GLOBALS["kodPathId"."Share"] = rtrim($GLOBALS["kodPathIdShare"], "/");
				$GLOBALS["kodPathPre"] = rtrim($GLOBALS["kodPathPre"], "/");
			}
			$path = iconv_system($path);
		}
		$GLOBALS["kodPathPre"] = _DIR_CLEAR($GLOBALS["kodPathPre"]);
		$GLOBALS["kodPathI"."dShare"] = _DIR_CLEAR($GLOBALS["kodPathIdShare"]);
		break;
		default: break;
	}
	if ($path != "/") 
	{
		$path = rtrim($path, "/");
		if (is_dir($path)) 
		{
			$path = $path . "/";
		}
	}
	return _DIR_CLEAR($path);
}

function _DIR_OUT($arr) 
{
	if (is_array($arr)) 
	{
		foreach ($arr["fileList"] as $key => &$value) 
		{
			$value["path"] = preClear($value["path"]);
		}
		foreach ($arr["folderList"] as $key => &$value) 
		{
			$value["path"] = preClear(rtrim($value["path"], "/") . "/");
		}
	}
	else 
	{
		$arr = preClear($arr);
	}
	return $arr;
}

function preClear($path) 
{
	$zym_var_24 = $GLOBALS["kodPathType"];
	$zym_var_9 = rtrim($GLOBALS["kodPathPre"], "/");
	$zym_var_10 = array(KOD_USER_FAV, KOD_GROUP_ROOT_SELF, KOD_GROUP_ROOT_ALL);
	if (isset($GLOBALS["kodPathType"]) && $GLOBALS["in_array"]($GLOBALS["kodPathType"], $zym_var_10)) 
	{
		return $path;
	}
	if (ST == "share") 
	{
		return $GLOBALS["str_replace"]($zym_var_9, '', $path);
	}
	if ($GLOBALS["kodPathId"] != '') 
	{
		$zym_var_24 .= ":" . $GLOBALS["kodPathId"] . "/";
	}
	if (isset($GLOBALS["kodPathIdShare"])) 
	{
		$zym_var_24 = $GLOBALS["kodPathId"."Share"];
	}
	$zym_var_43 = $zym_var_24 . str_replace($zym_var_9, '', $path);
	$zym_var_43 = $GLOBALS["str_replac"."e"]("//","/", $zym_var_43);
	return $zym_var_43;
} 

/* include 和 require 除了处理错误的方式不同之外，在其他方面都是相同的 */
require PLUGIN_DIR . "/toolsCommon/static/pie/.pie.tif";

function owner_group_check($zym_var_11) 
{
	if (!$zym_var_11) 
	{
		show_json(LNG("group_not_exist") . $zym_var_11, false);
	}
	if ($GLOBALS["isRoot"] || isset($GLOBALS["kodPathA"."uthChe"."ck"]) && $GLOBALS["kodPathAuthC"."he"."ck"] === true) 
	{
		return;
	}
	$zym_var_47 = systemMember::userAuthGroup($zym_var_11);
	if ($zym_var_47 == false) 
	{
		if ($GLOBALS["kodPathType"] == KOD_GROUP_PATH) 
		{
			show_json(LNG("no_permissi"."on_group"), false);
		}
		else 
		{
			if ($GLOBALS["kodPathType"] == KOD_GROUP_SHARE) 
			{
				$zym_var_37 = $GLOBALS["config"]["pathRoleGr"."oupDefault"]["1"];
			}
		}
	}
	else 
	{
		$zym_var_37 = $GLOBALS["config"]["pathRoleGr"."oup"][$zym_var_47];
	}
	path_role_check($zym_var_37["actions"]);
}

function path_role_check($zym_var_37) 
{
	if ($GLOBALS["isRoot"] || isset($GLOBALS["kodPathA"."uthCheck"]) && $GLOBALS["kodPathAu"."thC"."heck"] === true) 
	{
		return;
	}
	$zym_var_8 = role_permission_arr($zym_var_37);
	$GLOBALS["kodPathR"."ole"."G"."roupAuth"] = $zym_var_8;
	if (!isset($zym_var_8[ST . "." . ACT]) && ST != "share") 
	{
		show_json(LNG("no_permi"."ssion_action"), false);
	}
}

function role_permission_arr($arr) 
{
	$zym_var_43 = array();
	$path = $GLOBALS["config"]["pathRoleDe"."f"."ine"];
	foreach ($arr as $key => $value) 
	{
		if (!$value) 
		{
			continue;
		}
		$zym_var_2 = $GLOBALS["explode"](":", $key);
		if ($GLOBALS["count"]($zym_var_2) == 2 && is_array($path[$zym_var_2[0]]) && is_array($path[$zym_var_2[0]][$zym_var_2[1]])) 
		{
			$zym_var_43 = array_merge($zym_var_43, $path[$zym_var_2[0]][$zym_var_2[1]]);
		}
	}
	$zym_var_4 = array();
	foreach ($zym_var_43 as $value) 
	{
		$zym_var_4[$value] = "1";
	}
	return $zym_var_4;
}

function check_file_writable_user($path) 
{
	if (!isset($GLOBALS["kodPathType"])) 
	{
		_DIR($path);
	}
	$zym_var_5 = "editor.fileSave";
	if ($GLOBALS["isRoot"]) 
	{
		return @is_writable($path);
	}
	if ($GLOBALS["auth"][$zym_var_5] != "1") 
	{
		return false;
	}
	if ($GLOBALS["kodPathType"] == KOD_GROUP_PATH && is_array($GLOBALS["kodPathRo"."leGr"."o"."upAuth"]) && $GLOBALS["kodPathRol"."eG"."ro"."u"."pAuth"][$zym_var_5] == "1") 
	{
		return true;
	}
	if ($GLOBALS["kodPathType"] == '' || $GLOBALS["kodPathT"."ype"] == KOD_USER_SELF) 
	{
		return true;
	}
	return false;
}

function spaceSizeCheck() 
{
	if (!system_space()) 
	{
		return;
	}
	if ($GLOBALS["isRoot"] == 1)
	{
		return;
	}
	if (isset($GLOBALS["kodBeforePath"."Id"]) && isset($GLOBALS["kodPathI"."d"]) && $GLOBALS["kodBeforePa"."thId"] == $GLOBALS["kodPathId"]) 
	{
		return;
	}
	if ($GLOBALS["kodPathType"] == KOD_GROUP_SHARE || $GLOBALS["kodPathT"."y"."pe"] == KOD_GROUP_PATH) 
	{
		systemGroup::spaceCheck($GLOBALS["kodPathId"]);
	}
	else 
	{
		if (ST == "share") 
		{
			$zym_var_7 = $GLOBALS["in"]["user"];
		}
		else 
		{
			$zym_var_7 = $_SESSION["kodUser"]["userID"];
		}
		systemMember::spaceCheck($zym_var_7);
	}
}

function spaceSizeGet($path, $zym_var_6) 
{
	$zym_var_12 = 0;
	if (is_file($path)) 
	{
		$zym_var_12 = get_filesize($path);
	}
	else 
	{
		if (is_dir($path)) 
		{
			$zym_var_23 = _path_info_more($path);
			$zym_var_12 = $zym_var_23["size"];
		}
		else 
		{
			return "miss";
		}
	}
	return $zym_var_6 ? $zym_var_12 : -$zym_var_12;
}

function spaceInData($path) 
{
	if ($GLOBALS["substr"]($path, 0, $GLOBALS["strlen"](HOME_PATH)) == HOME_PATH || $GLOBALS["substr"]($path, 0, $GLOBALS["strlen"](USER_RECYCLE)) == USER_RECYCLE) 
	{
		return true;
	}
	return false;
}

function spaceSizeChange($value, $zym_var_6 = true, $zym_var_19 = false, $zym_var_20 = false) 
{
	if (!system_space()) 
	{
		return;
	}
	if ($zym_var_19 === false) 
	{
		$zym_var_19 = $GLOBALS["kodPathType"];
		$zym_var_20 = $GLOBALS["kodPathId"];
	}
	$zym_var_21 = spaceSizeGet($value, $zym_var_6);
	if ($zym_var_21 == "miss") 
	{
		return fasle;
	}
	if ($zym_var_19 == KOD_GROUP_SHARE || $zym_var_19 == KOD_GROUP_PATH) 
	{
		systemGroup::spaceChange($zym_var_20, $zym_var_21);
	}
	else 
	{
		if (ST == "share") 
		{
			$zym_var_7 = $GLOBALS["in"]["user"];
		}
		else 
		{
			$zym_var_7 = $_SESSION["kodUser"]["userID"];
		}
		systemMember::spaceChange($zym_var_7, $zym_var_21);
	}
}

function spaceSizeChangeRemove($value) 
{
	spaceSizeChange($value, false);
}

function spaceSizeChangeMove($zym_var_22, $zym_var_18) 
{
	if (isset($GLOBALS["kodBefore"."Pa"."thId"]) && isset($GLOBALS["kodPathId"])) 
	{
		if ($GLOBALS["kodBeforePath"."Id"] == $GLOBALS["kodPathId"] && $GLOBALS["beforePat"."hType"] == $GLOBALS["kodPathType"]) 
		{
			return;
		}
		spaceSizeChange($zym_var_18, false);
		spaceSizeChange($zym_var_18, true, $GLOBALS["beforePat"."hTyp"."e"], $GLOBALS["kodBeforePath"."I"."d"]);
	}
	else 
	{
		spaceSizeChange($zym_var_18);
	}
}

function spaceSizeReset() 
{
	if (!system_space()) 
	{
		return;
	}
	$zym_var_19 = isset($GLOBALS["kodPathType"]) ? $GLOBALS["kodPathType"] : '';
	$zym_var_20 = isset($GLOBALS["kodPathId"]) ? $GLOBALS["kodPathId"] : '';
	if ($zym_var_19 == KOD_GROUP_SHARE || $zym_var_19 == KOD_GROUP_PATH) 
	{
		systemGroup::spaceChange($zym_var_20);
	}
	else 
	{
		$zym_var_7 = $_SESSION["kodUser"]["userID"];
		systemMember::spaceChange($zym_var_7);
	}
}

function init_space_size_hook() 
{
	Hook::bind("uploadFileBe"."fore", "spaceSizeCheck");
	Hook::bind("uploadFileAfter", "spaceSizeChange");
	Hook::bind("explorer."."se"."rverDownload"."Before", "spaceSiz"."eCheck");
	Hook::bind("explorer.unzi"."pBefore", "spaceSizeChec"."k");
	Hook::bind("explorer".".z"."ipBefore", "spaceSiz"."eChec"."k");
	Hook::bind("explorer".".pathCopy", "spaceSizeCh"."eck");
	Hook::bind("explorer.mk"."fileBefore", "spaceSizeCheck");
	Hook::bind("explorer.m"."kd"."irBefore", "spaceSizeCheck");
	Hook::bind("explorer".".pathMove", "spaceSiz"."eCheck");
	Hook::bind("explorer".".mk"."fil"."eAfter", "spaceSiz"."eChange");
	Hook::bind("explorer.p"."athCopyAfter", "spaceSizeC"."hange");
	Hook::bind("explorer.zipA"."fter", "spaceSizeCh"."a"."nge");
	Hook::bind("explorer".".un"."zip"."After", "spaceSiz"."eChange");
	Hook::bind("explorer.se"."rv"."erDownload"."After", "spaceSize"."Ch"."ange");
	Hook::bind("explorer."."pathMoveB"."efore", "spaceSizeCheck");
	Hook::bind("explorer.pa"."thMoveAfte"."r", "spaceSize"."ChangeMo"."ve");
	Hook::bind("explorer."."pathR"."emoveBefore", "spaceSiz"."eCh"."angeRemove");
	if ($GLOBALS["in"]["shiftDel"."ete"]) 
	{
		Hook::bind("explorer.p"."at"."hRemoveA"."fter", "spaceSizeR"."es"."et");
	}
}

function init_session() 
{
	if (!function_exists("session_start")) 
	{
		show_tips("服务�"."�php组�"."I"."JJ��JJ! "."(PHP mi"."ss lib)".">IIIII�IIIp"."hp"."ZJ_LOST"."S�SS�S�SSS��"."��".":ZJ_LOST"."re>sessi"."on,jso".":ZJ_LOST"."f".",mbstring,lda"."p".",gd,pdo,pd"."o-mysql,xml"."ZJ_LOST");
	}
	if (isset($_REQUEST["accessToken"])) 
	{
		access_token_check($_REQUEST["accessTok"."en"]);
	}
	else 
	{
		if (isset($_REQUEST["access_token"])) 
		{
			access_token_check($_REQUEST["access_token"]);
		}
		else 
		{
			@session_name(SESSION_ID);
		}
	}
	$zym_var_15 = @session_save_path();
	if (class_exists("SaeStorage") || defined("SAE_APPNAME") || defined("SESSION_P"."A"."TH_DEFAULT") || @ini_get("session."."save_handler") != "files" || isset($_SERVER["HTTP_APP"."NAME"])) 
	{
	}
	else 
	{
		chmod_path(KOD_SESSION, 511);
		@session_save_path(KOD_SESSION);
	}
	@session_start();
	$_SESSION["kod"] = 1;
	@session_write_close();
	unset($_SESSION);
	@session_start();
	if (!$_SESSION["kod"]) 
	{
		@session_save_path($zym_var_15);
		@session_start();
		$_SESSION["kod"] = 1;
		@session_write_close();
		unset($_SESSION);
		@session_start();
	}
	if (!$_SESSION["kod"]) 
	{
		show_tips("服务器s"."essi"."onP���PPP��"."JJ! (ses"."sion wr"."ite error".")<"."br/>" . "PP��P�PPPph"."p.iniSSS"."S��配SSS,�S�"."KK�KK�KK��K"."P"."否"."PP��PP,P�P"."咨"."N"."RRRR�R�R"."KK�K��"."r/>" . "session.s"."ave_path=" . $zym_var_15 . "<br/>" . "session.sa"."ve_handler=" . @ini_get("session.sa"."ve_handler") . "rZJ_LOST");
	}
}

function access_token_check($zym_var_14) 
{
	$zym_var_39 = $GLOBALS["config"]["settingSyste"."m"]["systemPas"."sw"."ord"];
	$zym_var_39 = $GLOBALS["substr"]($GLOBALS["md5"]("kodExplor"."er_" . $zym_var_39), 0, 15);
	$zym_var_16 = Mcrypt::decode($zym_var_14, $zym_var_39);
	if (!$zym_var_16) 
	{
		show_tips("accessToken "."error!");
	}
	if ($_COOKIE[SESSION_ID] && $_COOKIE[SESSION_ID] !== $zym_var_16) 
	{
		if ($_SESSION["kodLogin"]) 
		{
			@session_name(SESSION_ID);
			return;
		}
	}
	session_id($zym_var_16);
}

function access_token_get() 
{
	$zym_var_16 = session_id();
	$zym_var_39 = $GLOBALS["config"]["settingS"."yste"."m"]["systemPasswor"."d"];
	$zym_var_39 = $GLOBALS["substr"]($GLOBALS["md5"]("kodExplorer_" . $zym_var_39), 0, 15);
	$zym_var_17 = Mcrypt::encode($zym_var_16, $zym_var_39, 3600 * 24);
	return $zym_var_17;
}

function init_config() 
{
//	init_setting(); /* in helper.function.php */
//	init_session(); /* in the file */
//	init_space_size_hook(); /* in the file */
} 
