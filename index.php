<?php
/**
 * index.php
 * 从最简单的MVC框架开始，一步一步理解和搭建KODExplorer可道云
 *
 * @author langcai1943
 */

    include ('config/config.php');

    $app = new Application();
    $app->setDefaultController('desktop');
    $app->setDefaultAction('index');
    $app->run();
?>
