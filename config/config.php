<?php
/*
* @link http://kodcloud.com/
* @author warlee | e-mail:kodcloud@qq.com
* @copyright warlee 2014.(Shanghai)Co.,Ltd
* @license http://kodcloud.com/tools/license/license.txt
*/

@date_default_timezone_set(@date_default_timezone_get());
@set_time_limit(1200);//20min pathInfoMuti,search,upload,download...
@ini_set("max_execution_time",1200);
@ini_set('memory_limit','500M');//
@ini_set('session.cache_expire',1800);

@ini_set("display_errors","on");
@error_reporting(E_ALL^E_NOTICE);//

header("Content-type: text/html; charset=utf-8");
define('BASIC_PATH',str_replace('\\','/',dirname(dirname(__FILE__))).'/');
define('LIB_DIR',       BASIC_PATH .'app/');        //系统库目录
define('CONTROLLER_DIR',LIB_DIR .'controller/'); 	//控制器目录
define('MODEL_DIR',     LIB_DIR .'model/');  		//模型目录
define('TEMPLATE',      LIB_DIR .'template/');   	//模版文件路径
define('CORER_DIR',		LIB_DIR .'core/');			//核心目录

function myAutoloader($name) {
    $find = array(
        CORER_DIR.$name.'.class.php',
        MODEL_DIR.$name.'.class.php',
        CONTROLLER_DIR.$name.'.class.php',
    );
    foreach ($find as $file) {
        if(is_file($file)){
            include_once($file);
            return true;
        }
    }
    return false;
}
/* 当我们new一个类，并且这个类文件没有被包含时候就会执行这个
   myAutoloader方法，方便我们增加MVC模块时不用修改包含文件的代码 */
spl_autoload_register('myAutoloader', true, true);

