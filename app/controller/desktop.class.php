<?php
/*
* @link http://kodcloud.com/
* @author warlee | e-mail:kodcloud@qq.com
* @copyright warlee 2014.(Shanghai)Co.,Ltd
* @license http://kodcloud.com/tools/license/license.txt
*/

class desktop extends Controller{
	function __construct() {
		parent::__construct();
	}
	public function index() {
		$this->display('index.html');
	}
}
