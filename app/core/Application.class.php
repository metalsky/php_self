<?php
/*
* @link http://kodcloud.com/
* @author warlee | e-mail:kodcloud@qq.com
* @copyright warlee 2014.(Shanghai)Co.,Ltd
* @license http://kodcloud.com/tools/license/license.txt
*/

/**
 * 程序路由处理类
 * 这里类判断外界参数调用内部方法
 */
class Application {
	private $defaultController = null;	//默认的类名
	private $defaultAction = null;		//默认的方法名
	public $subDir ='';					//控制器子目录
	public $model = '';					//控制器对应模型  对象。
	
	/**
	 * 设置默认的类名
	 * @param string $defaultController 
	 */
	public function setDefaultController($defaultController){
		$this -> defaultController = $defaultController;
	} 

	/**
	 * 设置默认的方法名
	 * @param string $defaultAction 
	 */
	public function setDefaultAction($defaultAction){
		$this -> defaultAction = $defaultAction;
	} 

	/**
	 * 设置控制器子目录
	 * @param string $dir 
	 */
	public function setSubDir($dir){
		$this -> subDir = $dir;
	} 

	/**
	 * 运行controller 的方法
	 * @param $class , controller类名。
	 * @param $function , 方法名
	 */
	public function appRun($class,$function){
		$subDir = $this -> subDir ? $this -> subDir . '/' : '';
		$classFile = CONTROLLER_DIR . $subDir.$class.'.class.php';
		$className = $class;//.'Controller'
		if (!class_exists($className)) {
		    include_once($classFile);
		}
		$instance = new $className();
		return $instance -> $function();
	}

	/**
	 * 调用实际类和方式
	 */
	public function run(){
		$st = $this->defaultController;
		$act = $this->defaultAction;

		define('ST',$st);
		define('ACT',$act);
		$this->appRun(ST,ACT);
	}
} 
